<?php 

require_once('SQLHelper.php');

class User {

	private $sql_obj = null;

	public function __construct(){

		$this->sql_obj = SQLHelper::get_instance();
	}

	public function login($username, $password) {
		$return = array();
		$return['success'] = false;
		$password = sha1($password);

 		$sql = "CALL validate_user('{$username}','{$password}')";
		$result = $this->sql_obj->CALL($sql);
		 
 		if($result){
			$rows = mysqli_fetch_assoc($result);

 			$return['success'] = $rows && sizeof($rows);
 			$return['data'] = $rows;
		}
		 
 		echo json_encode($return);
	}

	public function get() {
		$return = array();
		$return['success'] = false;

 		$sql = "CALL get_users()";
		$result = $this->sql_obj->CALL($sql);
		 
 		if($result){
			$final_data = array();
			while($row = mysqli_fetch_assoc($result)){
				$final_data[] = $row;
			}

 			$return['success'] = $final_data && sizeof($final_data);
 			$return['data'] = $final_data;
		}
		 
 		echo json_encode($return);
	}

	
	public function getUserTypes() {
		$return = array();
		$return['success'] = false;

 		$sql = "CALL get_user_type()";
		$result = $this->sql_obj->CALL($sql);
		 
 		if($result){
			$final_data = array();
			while($row = mysqli_fetch_assoc($result)){
				$final_data[] = $row;
			}

 			$return['success'] = $final_data && sizeof($final_data);
 			$return['data'] = $final_data;
		}
		 
 		echo json_encode($return);
	}

	public function add($data){
		$return = array();
		$return['success'] = false;

		$data['user_password'] = $password = sha1($data['user_password']);

		$sql = "CALL add_user('{$data['user_username']}', '{$data['user_password']}', '{$data['user_firstname']}', '{$data['user_lastname']}', '{$data['user_contact']}', '{$data['user_type_id']}','{$data['user_birth_date']}', '{$data['user_gender']}', '{$data['user_school_id']}')";

		if($this->sql_obj->CALL($sql)){
			$return['success'] = true;
		}
		echo json_encode($return);
	}

	public function delete($data){
		$return = array();
		$return['success'] = false;

		$sql = "CALL delete_user('{$$data['user_id']}')";

		if($this->sql_obj->CALL($sql)){
			$return['success'] = true;
		}
		echo json_encode($return);
	}

	public function update($data){
		$return = array();
		$return['success'] = false;

		$sql = "CALL update_user('{$data['user_school_id']}', '{$data['user_username']}', '{$data['user_firstname']}', '{$data['user_lastname']}', '{$data['user_contact']}', '{$data['user_type_id']}','{$data['user_birth_date']}', '{$data['user_gender']}', '{$data['user_id']}' )";

		if($this->sql_obj->CALL($sql)){
			$return['success'] = true;
		}
		echo json_encode($return);
	}

	public function updatePassword($data){
		$return = array();
		$return['success'] = false;

		$data['user_password'] = $password = sha1($data['user_password']);

		$sql = "CALL update_user_password('".$data['user_id']."','".$data['user_password']."')";

		if($this->sql_obj->CALL($sql)){
			$return['success'] = true;
		}
		echo json_encode($return);
	}
}



