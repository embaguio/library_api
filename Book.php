<?php 

require_once('SQLHelper.php');

class Book {

	private $sql_obj = null;

	public function __construct(){

		$this->sql_obj = SQLHelper::get_instance();
	}


	public function get() {
		$return = array();
		$return['success'] = false;

 		$sql = "CALL get_books()";
		$result = $this->sql_obj->CALL($sql);
		 
 		if($result){
			$final_data = array();
			while($row = mysqli_fetch_assoc($result)){
				$final_data[] = $row;
			}

 			$return['success'] = $final_data && sizeof($final_data);
 			$return['data'] = $final_data;
		}
		 
 		echo json_encode($return);
	}
	
	public function getStudentBooks($data) {
		$return = array();
		$return['success'] = false;

 		$sql = "CALL get_student_books({$data['id']})";
		$result = $this->sql_obj->CALL($sql);
		 
 		if($result){
			$final_data = array();
			while($row = mysqli_fetch_assoc($result)){
				$final_data[] = $row;
			}

 			$return['success'] = $final_data && sizeof($final_data);
 			$return['data'] = $final_data;
		}
		 
 		echo json_encode($return);
	}

	public function getGenre() {
		$return = array();
		$return['success'] = false;

 		$sql = "CALL get_genres()";
		$result = $this->sql_obj->CALL($sql);
		 
 		if($result){
			$final_data = array();
			while($row = mysqli_fetch_assoc($result)){
				$final_data[] = $row;
			}

 			$return['success'] = $final_data && sizeof($final_data);
 			$return['data'] = $final_data;
		}
		 
 		echo json_encode($return);
	}


	public function add($data){
		$return = array();
		$return['success'] = false;

		$sql = "CALL add_book('{$data['book_isbn']}','{$data['book_name']}','{$data['book_author']}','{$data['book_date_published']}','{$data['book_publisher']}','{$data['book_pages']}','{$data['genre_id']}')";

		if($this->sql_obj->CALL($sql)){
			$return['success'] = true;
		}
		echo json_encode($return);
	}

	public function updateStatus($data){
		$return = array();
		$return['success'] = false;

		$sql = "CALL update_book_status('{$data['book_id']}', '{$data['is_active']}')";

		if($this->sql_obj->CALL($sql)){
			$return['success'] = true;
		}
		echo json_encode($return);
	}

	public function update($data){
		$return = array();
		$return['success'] = false;

		$sql = "CALL update_book('{$data['book_id']}','{$data['book_isbn']}','{$data['book_name']}','{$data['book_author']}','{$data['book_date_published']}','{$data['book_publisher']}','{$data['book_pages']}','{$data['genre_id']}')";

		if($this->sql_obj->CALL($sql)){
			$return['success'] = true;
		}
		echo json_encode($return);
	}
}



