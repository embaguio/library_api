<?php

class Routes {
	public static function executeRoute($route) {

		$modulePath = ucwords($route["module"]).".php";
		$module = strtolower($route["module"]);
		$action = strtolower($route["action"]);


		if(!file_exists($modulePath)){
			Routes::handleError();
		}

		require_once($modulePath);
		Routes::executeAction($module, $action);
	}

	protected static function executeAction($module, $action) {
		$endpoint = "/".$module."/".$action;

		if($module == "user"){
			$action = new User();
		} else if($module == "book"){
			$action = new Book();
		} else if($module == "borrow"){
			$action = new Borrow();
		}  
		// else { 
		// 	$action = new Statics();
		// }
		
		switch ($endpoint) {
			/*
			** THIS CONSIST OF ROUTES FROM ENDPOINT URL
			** THIS WILL ONLY CALL CORRESPONDING METHODS IN CLASS
			*/
			case '/user/login':
					$action->login($_POST['username'], $_POST['password']);
				break;

			case '/user/get':
					$action->get();
				break;
			
			case '/user/get-user-types':
				$action->getUserTypes();
			break;
			
			case '/user/add':
				$action->add($_POST);
			break;
			
			case '/user/delete':
				$action->delete($_POST);
			break;
			
			case '/user/update':
				$action->update($_POST);
			break;

			case '/user/update-password':
				$action->updatePassword($_POST);
			break;


			
			case '/book/get':
				$action->get();
			break;
			
			case '/book/get-student-books':
				$action->getStudentBooks($_POST);
			break;

			case '/book/get-genre':
				$action->getGenre();
			break;

			case '/book/add':
				$action->add($_POST);
			break;

			case '/book/update-status':
				$action->updateStatus($_POST);
			break;

			case '/book/update':
				$action->update($_POST);
			break;
		

			case '/borrow/get':
				$action->get();
			break;

			case '/borrow/get-lines':
				$action->getLines($_POST);
			break;

			case '/borrow/update-lines':
				$action->updateLines($_POST);
			break;

			case '/borrow/add':
				$action->add($_POST);
			break;

			case '/borrow/add-lines':
				$action->addLines($_POST);
			break;

			case '/borrow/print':
				$action->print($_POST);
			break;

			default:
				Routes::handleError();
				break;
		}
	}

	public static function handleError() {
		http_response_code(404);
		$response = array();
		$response["error"] = "Resource Not Found";
		die(json_encode($response));
	}
}