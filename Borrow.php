<?php 
require 'vendor/autoload.php';
require_once 'vendor/dompdf/dompdf/lib/html5lib/Parser.php';
require_once 'vendor/dompdf/dompdf/src/Autoloader.php';
Dompdf\Autoloader::register();
use Dompdf\Dompdf;
require_once('SQLHelper.php');

class Borrow {

	private $sql_obj = null;

	public function __construct(){

		$this->sql_obj = SQLHelper::get_instance();
	}


	public function get() {
		$return = array();
		$return['success'] = false;

 		$sql = "CALL get_borrows()";
		$result = $this->sql_obj->CALL($sql);
		 
 		if($result){
			$final_data = array();
			while($row = mysqli_fetch_assoc($result)){
				$final_data[] = $row;
			}

 			$return['success'] = $final_data && sizeof($final_data);
 			$return['data'] = $final_data;
		}
		 
 		echo json_encode($return);
	}
	
	public function getLines($data, $isArray = false) {
		$return = array();
		$return['success'] = false;

 		$sql = "CALL get_borrow_lines({$data['borrow_id']})";
		$result = $this->sql_obj->CALL($sql);
		 
 		if($result){
			$final_data = array();
			while($row = mysqli_fetch_assoc($result)){
				$final_data[] = $row;
			}

 			$return['success'] = $final_data && sizeof($final_data);
 			$return['data'] = $final_data;
		}
		if ($isArray) {
			return $return;
		} else {
			echo json_encode($return);
		}
	}

	public function updateLines($data){
		$return = array();
		$return['success'] = false;
		$returned = 0;

		foreach ($data as $line) {
			if (isset($line['borrow_line_id']) && isset($line['borrow_line_returned'])) {
			
				$sql = "CALL update_borrow_line('{$line['borrow_line_id']}', '{$line['borrow_line_returned']}')";
				
				if($this->sql_obj->CALL($sql)){
					$returned+=$line['borrow_line_returned'];
					$return['success'] = true;
				}
			}
		}

		$sql = "CALL update_returned('{$data['borrow']['borrow_id']}','{$returned}')";
		$result = $this->sql_obj->CALL($sql);

		echo json_encode($return);
	}

	public function add($data){
		$return = array();
		$return['success'] = false;
		
		$sql = "CALL add_borrow('{$data['borrow_date']}','{$data['borrow_due_date']}','{$data['user_id']}','{$data['total_books_borrowed']}', @out_borrow_id)";

		$result = $this->sql_obj->CALL_OUT($sql, 'out_borrow_id');

 		if($result){
 			$return['success'] = true;
 			$return['data'] = $result;
		}

		echo json_encode($return);
	}

	public function addLines($data){
		$return = array();
		$return['success'] = false;

		foreach ($data as $line) {
			if (isset($line['book_id'])) {
			
				$sql = "CALL add_borrow_line('{$data['borrow_id']}','{$line['book_id']}','{$line['borrow_line_qty']}')";
	
				if($this->sql_obj->CALL($sql)){
					$return['success'] = true;
				}
			}
		}

		echo json_encode($return);
	}

	public function updateStatus($data){
		$return = array();
		$return['success'] = false;

		$sql = "CALL update_borrow_status('{$data['book_id']}', '{$data['is_active']}')";

		if($this->sql_obj->CALL($sql)){
			$return['success'] = true;
		}
		echo json_encode($return);
	}

	public function print($data) {
		$return = array();
		$return['success'] = false;
		$generatedDate = date('m/d/Y');
		$rows = $this->getLines($data, true)['data'];


		$buildRows = '';

		foreach($rows as $line) {
			
			$buildRows.="
				<tr>
					<td>{$line['book_isbn']}</td>
					<td>{$line['book_name']}</td>
					<td>{$line['borrow_line_qty']}</td>
					<td>{$line['borrow_line_returned']}</td>
				</tr>";
		}


		$htmlString = "<!DOCTYPE html>
		<html>
			<style>
				body {
					font-family: sans-serif;
      				margin: -20px 0px;
				}
				.pull-right {
					float: right;
				}
				.gen-details span {
					margin-right: 20px;
				}
				.company {
					background: #4292bb;
					color: white;
					padding: 20px;
				}
				.header {
					font-weight: bold;
					background: #4292bb;
					color: white;

				}

				td {
					padding: 10px;
				}

				tr:nth-child(even) {background-color: #f2f2f2;}
			</style>
		<body>
		
			<h3 class='company'>Borrow Transaction #{$data['borrow_id']} <span class='pull-right'>Date Generated: {$generatedDate}</span></h3>
			<div class='gen-details'>
				<h4>Borrower: {$data['user_school_id']} - {$data['user_firstname']} {$data['user_lastname']}</h4>
				<span><b>Date Borrowed:</b> {$data['borrow_date']}</span>
				<span><b>Due Date:</b> {$data['borrow_due_date']}</span>
			</div>
			<hr>
		
		
			<table style='width:100%'>
				<tr class='header'>
					<td>ISBN</td>
					<td>Book Name</td>
					<td>Borrowed Qty</td>
					<td>Returned Qty</td>
				</tr>
				{$buildRows}
			</table>
	
			<hr>
			<p style='text-align: right;'><b>Total Borrowed: </b> {$data['total_books_borrowed']}</p>
			<p style='text-align: right;'><b>Total Returned: </b> {$data['total_books_returned']}</p>
		
		</body>
		
		</html>";
		
		$curr_date =  date('Y-m-d');

		$dompdf = new DOMPDF();
		$dompdf->load_html($htmlString);
		$dompdf->render();
		$output = $dompdf->output();

		$saveLink = "printables/TR-{$curr_date}.pdf";
		file_put_contents($saveLink, $output);

		$base_url= "http://".$_SERVER['SERVER_NAME'].dirname($_SERVER['PHP_SELF']);

		$return['success'] = true;
		$return['data'] = $base_url."/".$saveLink;

		echo json_encode($return, JSON_UNESCAPED_SLASHES);
	}
}



